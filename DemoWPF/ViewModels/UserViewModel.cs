﻿using DemoWPF.Models;
using DemoWPF.MVVM;
using System.Collections.ObjectModel;

namespace DemoWPF.ViewModels
{
    class UserViewModel : ViewModelBase
    {
        public ObservableCollection<User> Users { get; set; }
        public RelayCommand AddCommand => new(execute => AddUser());
        public RelayCommand RemoveCommand => new(execute => RemoveUser(), canExecute => SelectedUser != null);
        public UserViewModel()
        {
            Users = new ObservableCollection<User>
            {
                new User("Kurt", "Cobain"),
                new User("Michael", "Jackson"),
                new User("Bruce", "Springsteen"),
                new User("Bob", "Dylan"),
                new User("Freddie", "Mercury")
            };
        }

        User? _selectedUser;
        public User? SelectedUser
        {
            get { return _selectedUser; }
            set 
            { 
                _selectedUser = value; 
                OnPropertyChanged();
            }
        }

        void AddUser()
        {
            Users.Add(new User("John", "Doe"));
        }

        void RemoveUser()
        {
            Users.Remove(SelectedUser!);
        }
    }
}
