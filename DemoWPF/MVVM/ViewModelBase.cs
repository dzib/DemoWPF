﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace DemoWPF.MVVM
{
    internal class ViewModelBase : INotifyPropertyChanged
    {
        /*
        Déclare un événement public de type PropertyChangedEventHandler. 
        Cet événement est utilisé pour notifier les abonnés qu'une propriété a été modifiée. 
        L'opérateur ? après PropertyChangedEventHandler indique que l'événement peut être null, c'est-à-dire qu'il peut ne pas avoir d'abonnés.
        */
        public event PropertyChangedEventHandler? PropertyChanged;

        /*
        Déclare une méthode protégée appelée OnPropertyChanged avec un paramètre optionnel propertyName de type string. 
        Cette méthode est utilisée pour déclencher l'événement PropertyChanged et notifier les abonnés du changement de propriété. 
        L'attribut [CallerMemberName] permet de récupérer automatiquement le nom de la propriété qui a été modifiée. 
        */
        protected void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            /*
            Invoque l'événement PropertyChanged en vérifiant d'abord s'il y a des abonnés (PropertyChanged?). 
            Si l'événement n'est pas null, c'est-à-dire s'il y a des abonnés, alors il est déclenché. 
            L'objet courant (this) est utilisé comme source de l'événement, et une nouvelle instance de PropertyChangedEventArgs 
            est créée avec le nom de la propriété modifiée (propertyName). 
            Ainsi, les abonnés à cet événement seront informés que la propriété spécifiée a été modifiée.
            */
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
