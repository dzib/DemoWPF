﻿using System;
using System.Windows.Input;

namespace DemoWPF.MVVM
{
    /*
    Déclare une classe interne appelée RelayCommand qui implémente l'interface ICommand. 
    L'interface ICommand définit les méthodes et événements nécessaires pour implémenter des commandes qui peuvent être utilisées dans les applications, 
    notamment pour la liaison avec des éléments d'interface utilisateur.
    */
    internal class RelayCommand : ICommand
    {
        /*
        Déclarent deux champs privés execute et canExecute de types Action<object> et Func<object, bool> respectivement. 
        execute représente l'action à exécuter lorsque la commande est exécutée, 
        et canExecute représente une fonction qui détermine si la commande peut être exécutée dans son état actuel. 
        */
        Action<object> execute;
        Func<object, bool> canExecute;

        /*
        Définit un événement CanExecuteChanged qui est requis par l'interface ICommand. 
        L'événement utilise le gestionnaire d'événements statique CommandManager.RequerySuggested pour s'abonner et se désabonner
        des modifications pouvant affecter l'état d'exécution de la commande. 
        Cela permet de déclencher automatiquement la vérification de l'état de la commande lorsque des changements dans l'application peuvent influencer son exécution. 
        */
        public event EventHandler? CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        /*
        Permet de créer une nouvelle instance de RelayCommand. 
        Il prend en paramètre une action execute qui sera exécutée lorsque la commande est appelée, 
        et une fonction canExecute facultative qui détermine si la commande peut être exécutée. 
        Les paramètres sont ensuite affectés aux champs correspondants. 
        */
        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        /*
        Implémente la méthode CanExecute requise par l'interface ICommand. 
        Elle renvoie true si la fonction canExecute est null (ce qui signifie que la commande peut toujours être exécutée), 
        ou si la fonction canExecute renvoie true pour le paramètre donné. 
        */
        public bool CanExecute(object? parameter)
        {
            return canExecute == null || canExecute(parameter);
        }

        /*
        Implémente la méthode Execute requise par l'interface ICommand. 
        Elle exécute l'action execute avec le paramètre donné. 
        */
        public void Execute(object? parameter)
        {
            execute(parameter);
        }
    }
}
