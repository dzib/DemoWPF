﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace DemoCommunityWPF.Models
{
    public partial class User : ObservableObject
    {
        [ObservableProperty] string _firstName;
        [ObservableProperty] string _lastName;       

        public User(string firstName, string lastName)
        {
            _firstName = firstName;
            _lastName = lastName;
        }
    }
}
