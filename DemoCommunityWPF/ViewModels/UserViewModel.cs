﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using DemoCommunityWPF.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DemoCommunityWPF.ViewModels
{
    partial class UserViewModel : ObservableObject
    {
        [ObservableProperty] string? _firstName;
        [ObservableProperty] string? _lastName;
        [ObservableProperty][NotifyCanExecuteChangedFor(nameof(RemoveCommand))] User? _selectedUser;
        [ObservableProperty] IList<User> _users;

        public UserViewModel()
        {
            Users = new ObservableCollection<User>
            {
                new User("Kurt", "Cobain"),
                new User("Michael", "Jackson"),
                new User("Bruce", "Springsteen"),
                new User("Bob", "Dylan"),
                new User("Freddie", "Mercury")
            };
        }

        [RelayCommand]
        void Add() => Users.Add(new User("John", "Doe"));

        [RelayCommand(CanExecute = nameof(IsRemovable))]
        void Remove() => Users.Remove(SelectedUser!);
                
        bool IsRemovable() { return SelectedUser != null; }
    }
}
